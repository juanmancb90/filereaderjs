/**
 * [description]
 * @return {[type]} [description]
 */
(function() {
	/**
	 * [Reader description]
	 */
	self.Reader = function() {
		this.jq = $.noConflict();
	};

	/**
	 * [prototype description]
	 * @type {Object}
	 */
	self.Reader.prototype = {
		leerXML: function() {
			procesarXML(this.jq);
		},
		leerJson: function() {
			procesarJSON(this.jq);
		}
	};

	/**
	 * [procesarXML description]
	 * @param  {[type]} jq [description]
	 * @return {[type]}    [description]
	 */
	function procesarXML(jq)
	{
		var xHttp = new XMLHttpRequest();
		xHttp.onreadystatechange = function() {
			if (xHttp.readyState == 4 && xHttp.status == 200) {
				console.log("Cargo XML"+xHttp);
				leerXML(xHttp, jq);
			}
		};
		xHttp.open("GET", "data.xml", true);
		xHttp.send();
	}

	/**
	 * [leerXML description]
	 * @param  {[type]} xml    [description]
	 * @param  {[type]} jquery [description]
	 * @return {[type]}        [description]
	 */
	function leerXML(xml, jquery)
	{
		var x, xmlDoc, txt;
    xmlDoc = xml.responseXML;
    txt = "";
    x = xmlDoc.getElementsByTagName("title");
		for (var i = 0; i < x.length; i++) {
			txt += x[i].childNodes[0].nodeValue+'<br>';
		}
		jquery(document).ready(function() {
			jquery('#content').append(txt);
		});
	}

	/**
	 * [procesarJSON description]
	 * @param  {[type]} jq [description]
	 * @return {[type]}    [description]
	 */
	function procesarJSON(jq) 
	{
		jq(document).ready(function() {
			jq.ajax({
				url:'data.json',
				Type: 'GET',
				dataType: 'json',
				error: function(err) {
					console.log(err.toStrin());
				},
				success: function(data) {
					if (data !== null) {
						console.log("cargo json"+data);
						leerJSON(data, jq);
					}
				},
			});
		});
	}

	/**
	 * [leerJSON description]
	 * @param  {[type]} data [description]
	 * @param  {[type]} jq   [description]
	 * @return {[type]}      [description]
	 */
	function leerJSON(data, jq) 
	{
		var item = {};
		jq.each(data,function(k,e) {
			item[k] = e;
		});
		console.log(JSON.stringify(item));
		for (x in item) {
			jq(document).ready(function() {
				jq('#content2').append(item[x]);
				jq('#content2').append('<br>');
			});
		}
	}
})();

/**
 * [context description]
 * @type {Reader}
 */
var context = new Reader();
main();

/**
 * [main description]
 * @param  {[type]} context [description]
 * @return {[type]}         [description]
 */
function main(context){
	try
	{
		this.context.leerXML();
		this.context.leerJson();
	}
	catch(err) 
	{
		console.log(err.toString());
	}
}